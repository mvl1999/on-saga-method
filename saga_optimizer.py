import torch
from random import randint


class SAGAOptimizer:
    def __init__(self, x, functions, lr=1e-3):
        """
        :param functions: list of functions from torch tensors to scalar torch tensors
        :param x: torch.tensor to optimize
        :param lr: learning rate (step)
        """
        assert x.requires_grad

        self.functions = functions
        self.x = x
        self.n = len(functions)

        self.lr = lr

        self.grads = torch.empty(self.n, *x.shape, dtype=torch.float64)
        self.grad_delta_norm = 0

        for j in range(0, self.n):
            self.grads[j, ...] = self.grad(j)
        self.grad_sum = self.grads.sum(dim=0)

    def grad(self, j):
        if self.x.grad is not None:
            self.x.grad.zero_()
        self.functions[j](self.x).backward()
        return self.x.grad

    def step(self):
        j = randint(0, self.n - 1)

        grad_j_prev = self.grads[j].clone()
        grad_j_new = self.grads[j] = self.grad(j)
        grad_j_delta = grad_j_new - grad_j_prev

        with torch.no_grad():
            self.x -= self.lr * (grad_j_delta + self.grad_sum / self.n)
        self.grad_sum += grad_j_delta  # TODO?: error accumulation
        self.grad_delta_norm += torch.norm(grad_j_delta)